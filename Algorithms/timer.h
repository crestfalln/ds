// Copyright (C) 2021 Himanshu Gupta
// 
// This file is part of Data Structures.
// 
// Data Structures is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Data Structures is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.

#ifndef TIMER_H
#define TIMER_H

#include <chrono>
#include <cstdio>
#include <ctime>

class Timer
{
    std::chrono::_V2::system_clock::time_point start;
    std::chrono::_V2::system_clock::time_point stop;
    std::chrono::high_resolution_clock         clock;

public:
    Timer()
    {
		start = clock.now();
    }
	~Timer()
	{
		stop = clock.now();
		auto time = static_cast<double>((stop - start).count());
		fprintf(stderr, "%f" , time/1000);
		fflush(stderr);
	}
};

#endif