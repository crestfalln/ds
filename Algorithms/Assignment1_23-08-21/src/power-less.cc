// Copyright (C) 2021 Himanshu Gupta
// 
// This file is part of Data Structures.
// 
// Data Structures is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Data Structures is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.

#ifdef ALGORITHM
/*
int power(a , n):
    if(n == 1):
        return a
    res = power(a, n/2) * power(a, n/2)
    if(n%2 == 1):
        res = res * a
    return res
*/
#endif

#include "timer.h"
#include <iostream>
#include <string>
#include <gmpxx.h>

mpz_class power(mpz_class const & a, mpz_class const & n)
{
    if (n == 1)
        return a;
    mpz_class res = power(a, n / 2) * power(a, n / 2);
    if (n % 2 == 1)
        res *= a;
    return res;
}

int main(int argc, char** args)
{
    mpz_class a = 23, n;
    if (argc > 1)
    {
        n = args[1];
    }
    mpz_class res;
    {
        Timer timer;
        res = power(a, n);
    }
    std::cout << res << '\n';
}