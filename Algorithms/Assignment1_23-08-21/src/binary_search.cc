// Copyright (C) 2021 Himanshu Gupta
// 
// This file is part of Data Structures.
// 
// Data Structures is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Data Structures is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.

#ifdef ALGORITHM
/*
int binary_search(start, end, el):
	if(end == start)
		return -1
	mid_point = start + (end - start) / 2
	if(*mid_point = el):
		return mid_point - start
	if(*mid_point < el):
		return binary_search(mid_point , end , el)
	return binary_search(start, mid_point, el)
*/
#endif

#include "timer.h"
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <functional>
#include <string>

template <typename _T>
_T* binary_search(_T* start, _T* end, _T const& el)
{
    if (end == start)
        return nullptr;
    auto mid_point = start + (end - start) / 2;
    if (*mid_point == el)
        return mid_point;
    if (std::less<_T> {}(*mid_point, el))
        return binary_search(mid_point, end, el);
    return binary_search(start, mid_point, el);
}

int main(int argc, char** args)
{
    size_t size = 6;
    if (argc > 2)
    {
        size = std::stol(args[1]);
        srand(std::stoi(args[2]));
    }
    int* arr = new int[size];
    for (int i = 0; i < size; ++i)
        arr[i] = i;
    int  topick = rand() % size;
    int* res;
    {
        Timer timer;
        res = binary_search(arr, arr + size, arr[topick]);
    }
    if (res == arr + topick)
        //printf("%ld", res - arr);
    delete[] arr;
}