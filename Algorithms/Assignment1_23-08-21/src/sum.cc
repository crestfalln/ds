// Copyright (C) 2021 Himanshu Gupta
// 
// This file is part of Data Structures.
// 
// Data Structures is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Data Structures is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.

#ifdef ALGORITHM
/*
int sum(ls , len):
    if(len == 0):
        return ls[1]
    return ls[len] + sum(ls , len - 1)
*/
#endif

#include "cstdlib"
#include "timer.h"
#include <climits>
#include <cstdio>
#include <string>

int sum(int* arr, int len)
{
    if (len == 1)
        return arr[0];
    return arr[0] + sum(arr + 1, len - 1);
}

int main(int argc, char** args)
{
    int size = 5; //Default Size
    if (argc > 1) //Handle Arguments
        size = std::stoi(args[1]);
    int* arr = new int[size];
    for (int i = 0; i < size; i++) //Fill arr with random ints
    {
        arr[i] = rand() % 1000;
    }
    int res;
    {
        Timer timer;
        res = sum(arr, size);
    }
    printf("%d", res);
    delete[] arr;
}