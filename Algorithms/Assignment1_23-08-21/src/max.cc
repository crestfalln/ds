// Copyright (C) 2021 Himanshu Gupta
//
// This file is part of Data Structures.
//
// Data Structures is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Data Structures is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.

#ifdef ALGORITHM
/*
int max(ls , len):
	int res = ls[1];
	for(i in range(2,len+1)):
		if(res < ls[i]):
			res = ls[i]
	return res
*/
#endif

#include "timer.h"
#include <cstdio>
#include <cstdlib>
#include <climits>
#include <functional>
#include <string>

template <typename _T>
_T max(_T* arr, size_t len)
{
    if (len != 0)
    {
        _T res = arr[0];
        for (int i = 1; i < len; ++i)
        {
			if(std::less<_T>{}(res, arr[i]))
				res = arr[i];
        }
		return res;
    }
	return _T();
}

int main(int argc , char **args)
{
	size_t size = 5;
	if(argc > 2)
	{
		size = std::stoul(args[1]);
		srand(std::stoul(args[2]));
	}
	int *arr = new int[size];
	for(int i = 0 ; i < 5; ++i)	
		arr[i] = rand()%INT_MAX;
	int res;
	{
		Timer timer;
		res = max(arr , size);
	}
	printf("%d", res);
	delete [] arr;
}