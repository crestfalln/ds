// Copyright (C) 2021 Himanshu Gupta
// 
// This file is part of Data Structures.
// 
// Data Structures is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Data Structures is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.

#ifdef ALGORITM
/*
	int linear_search(start, end, el):
		if(last[1] == el):
			return last - start
		if(start == end)
			return -1
		return linear_search(start, end - 1, el)
*/
#endif

#include "timer.h"
#include <cstdio>
#include <string>

template <typename _T>
_T* recurse_linear_search(_T* start, _T* end, _T const& el)
{
    if (*start == el)
        return start;
    if (start == end)
        return nullptr;
    return recurse_linear_search(start + 1, end, el);
}

template <typename _T>
_T* linear_search(_T* start, _T* end, _T const& el)
{
    for (_T* i = start; start != end + 1; ++i)
    {
        if (*i == el)
            return i;
    }
    return nullptr;
}

int main(int argc, char** args)
{
    size_t size = 6;
    if (argc > 2)
    {
        size = std::stol(args[1]);
        srand(std::stoi(args[2]));
    }
    int* arr = new int[size];
    for (int i = 0; i < size; ++i)
        arr[i] = rand() % (size + 1000);
    int* res;
    int  topick = rand() % size;
    {
        Timer timer;
        res = linear_search(arr, arr + size, arr[topick]);
    }
    if (res == arr + topick)
        //printf("%ld" , res - arr );
        delete[] arr;
}