// Copyright (C) 2021 Himanshu Gupta
// 
// This file is part of Data Structures.
// 
// Data Structures is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Data Structures is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.

#ifdef ALGORITHM
/*
truth_table(iter, arr):
	if(iter == 0):
		print (arr)
		return
	for(i in range(0 , 1)):
		arr[iter] = i
		truth_table(iter - 1, arr)
	return
*/
#endif

#include "timer.h"
#include <array>
#include <iostream>
#include <string>

void print(bool* arr, size_t size)
{
    for (size_t i = 0; i < size; ++i)
    {
        if (arr[i] == 0)
            printf("F");
        else
            printf("T");
    }
    printf("\n");
}

void truth_table(bool* arr, size_t size, int iter = 0)
{
    if (iter == size)
    {
        //print(arr , size);
        return;
    }
    for (int i = 0; i < 2; ++i)
    {
        arr[iter] = i;
        truth_table(arr, size, iter + 1);
    }
    return;
}

int main(int argc, char* args[])
{
    int size = 2;
    if (argc > 1)
        size = std::stoi(args[1]);
    auto arr = new bool[size];
    {
        Timer timer;
        truth_table(arr, size);
    }
}