#!/bin/bash

# Copyright (C) 2021 Himanshu Gupta
# 
# This file is part of Data Structures.
# 
# Data Structures is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Data Structures is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.


TEMP=/tmp/time-search-$$
trap "rm $TEMP" EXIT ERR SIGINT SIGTERM SIGQUIT

for i in {1..1000}
do 
	$1 $2 $RANDOM ${@:2} 2>> $TEMP > /dev/null
	printf "\n" >> $TEMP
done 
printf "$2    " 1>&2
cat $TEMP | awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' 1>&2
exit 0