<!--
Copyright (C) 2021 Himanshu Gupta
 
 All the text and media in this file is licensed under an
 Creative Commons Attribution-ShareAlike 4.0 International License,
 and the code snippets are licensed under GPLv3.
 
 You should have received a copy of the license along with this
 work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
-->

# Data Sturctures Assignment 1

## Author: \[Himanshu Gupta\]

------------------------------------------------------------------------

## Binary Search

### Aim:

To find the position and existence of an element in an ordered list.

### Description:

The algorithm divides the given list in 2 halves in one step and
depending on conditions chooses one half and repeats until the element
is found.

### Use Cases:

Fast searching of indexed data. Used in search engines, filesystem
searching, databases, etc.

### Main:

``` py
int binary_search(start, end, el):
    if(end == start)
        return -1
    mid_point = start + (end - start) / 2
    if(*mid_point = el):
        return mid_point - start
    if(*mid_point < el):
        return binary_search(mid_point , end , el)
    return binary_search(start, mid_point, el)
```

### Implementation:

``` cxx
#include "timer.h"
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <functional>
#include <string>

template <typename _T>
_T* binary_search(_T* start, _T* end, _T const& el)
{
    if (end == start)
        return nullptr;
    auto mid_point = start + (end - start) / 2;
    if (*mid_point == el)
        return mid_point;
    if (std::less<_T> {}(*mid_point, el))
        return binary_search(mid_point, end, el);
    return binary_search(start, mid_point, el);
}

int main(int argc, char** args)
{
    size_t size = 6;
    if (argc > 2)
    {
        size = std::stol(args[1]); srand(std::stoi(args[2]));
    }
    int* arr = new int[size];
    for (int i = 0; i < size; ++i)
        arr[i] = i;
    int  topick = rand() % size;
    int* res;
    {
        Timer timer;
        res = binary_search(arr, arr + size, arr[topick]);
    }
    if (res == arr + topick)
        printf("%ld", res - arr);
    delete[] arr;
}
```

### Time Analysis:

The time complexity is O(*l**o**g*(*n*)), this can be verified with the
graph below.  
![time-analysis](../graph/binary_search.png) \* See Notes at the end of
this paper to read about the methodology of time measurements

------------------------------------------------------------------------

## Linear Search

### Aim:

To find the position and existence of an element in a list ordered or
unordered.

### Description:

The algorithm iterates through the entire list to find the element.

### Use Cases:

Slow searching of in-indexed data. Used in search through documents,
files, etc.

### Main:

``` py
    int linear_search(start, end, el):
        if(last[1] == el):
            return last - start
        if(start == end)
            return -1
        return linear_search(start, end - 1, el)
```

### Implementation:

``` cxx
#include "timer.h"
#include <cstdio>
#include <string>

template <typename _T>
_T* recurse_linear_search(_T* start, _T* end, _T const& el)
{
    if (*start == el)
        return start;
    if (start == end)
        return nullptr;
    return recurse_linear_search(start + 1, end, el);
}

template <typename _T>
_T* linear_search(_T* start, _T* end, _T const& el)
{
    for (_T* i = start; start != end + 1; ++i)
    {
        if (*i == el)
            return i;
    }
    return nullptr;
}

int main(int argc, char** args)
{
    size_t size = 6;
    if (argc > 2)
    {
        size = std::stol(args[1]);
        srand(std::stoi(args[2]));
    }
    int* arr = new int[size];
    for (int i = 0; i < size; ++i)
        arr[i] = rand() % (size + 1000);
    int* res;
    int  topick = rand() % size;
    {
        Timer timer;
        res = linear_search(arr, arr + size, arr[topick]);
    }
    if (res == arr + topick)
        printf("%ld" , res - arr );
        delete[] arr;
}
```

### Time Analysis:

The time complexity is O(*n*), this can be verified with the graph
below.  
![time-analysis](../graph/linear_search.png) \* See Notes at the end of
this paper to read about the methodology of time measurements

------------------------------------------------------------------------

## Sum of a list

### Aim:

To find the sum of a list.

### Description:

The algorithm recurses through a list removing one element each
recursive call until none remain where it returns 0.

### Use Cases:

Used to sum a list of numbers together. May be used in any number of
programs.

### Main:

``` py
int sum(ls , len):
    if(len == 0):
        return ls[1]
    return ls[len] + sum(ls , len - 1)
```

### Implementation:

``` cxx
#include "cstdlib"
#include "timer.h"
#include <climits>
#include <cstdio>
#include <string>

int sum(int* arr, int len)
{
    if (len == 1)
        return arr[0];
    return arr[0] + sum(arr + 1, len - 1);
}

int main(int argc, char** args)
{
    int size = 5; //Default Size
    if (argc > 1) //Handle Arguments
        size = std::stoi(args[1]);
    int* arr = new int[size];
    for (int i = 0; i < size; i++) //Fill arr with random ints
    {
        arr[i] = rand() % 1000;
    }
    int res;
    {
        Timer timer;
        res = sum(arr, size);
    }
    printf("%d", res);
    delete[] arr;
}
```

### Time Analysis:

The time complexity is O(*n*), this can be verified with the graph
below.  
![time-analysis](../graph/sum.png) \* See Notes at the end of this paper
to read about the methodology of time measurements

------------------------------------------------------------------------

## Truth Table

### Aim:

To print a truth table.

### Descirption:

The algorithm recurses through all the possibilities in a
lexicographical manner and then prints them.

### Use Cases:

Used of printing permutations of multiple objects with two values. For
example printing all possible permutation of 4 coins.

### Main:

``` py
truth_table(iter, arr):
    if(iter == 0):
        print (arr)
        return
    for(i in range(0 , 1)):
        arr[iter] = i
        truth_table(iter - 1, arr)
    return
```

### Implementation:

``` cxx
#include "timer.h"
#include <array>
#include <iostream>
#include <string>

void print(bool* arr, size_t size)
{
    for (size_t i = 0; i < size; ++i)
    {
        if (arr[i] == 0)
            printf("F");
        else
            printf("T");
    }
    printf("\n");
}

void truth_table(bool* arr, size_t size, int iter = 0)
{
    if (iter == size)
    {
        //print(arr , size);
        return;
    }
    for (int i = 0; i < 2; ++i)
    {
        arr[iter] = i;
        truth_table(arr, size, iter + 1);
    }
    return;
}

int main(int argc, char* args[])
{
    int size = 2;
    if (argc > 1)
        size = std::stoi(args[1]);
    auto arr = new bool[size];
    {
        Timer timer;
        truth_table(arr, size);
    }
}
```

### Time Analysis:

The time complexity is O(*e*<sup>*n*</sup>), this can be verified with
the graph below.  
![time-analysis](../graph/truth_table.png) \* See Notes at the end of
this paper to read about the methodology of time measurements

------------------------------------------------------------------------

## Power

### Aim:

To calculate *a*<sup>*n*</sup>.

### Description:

The algorithm lessens multiplications by calculating squares. This is
done through recursion.

### Use Cases:

Used for calculating exponents of numbers. Can be used in any number of
programs.

### Main:

``` py
int power(a , n):
    if(n == 1):
        return a
    res = power(a, n/2) * power(a, n/2)
    if(n%2 == 1):
        res = res * a
    return res
```

### Implementation:

``` cxx
#include "timer.h"
#include <iostream>
#include <string>
#include <gmpxx.h>

mpz_class power(mpz_class const & a, mpz_class const & n)
{
    if (n == 1)
        return a;
    mpz_class res = power(a, n / 2) * power(a, n / 2);
    if (n % 2 == 1)
        res *= a;
    return res;
}

int main(int argc, char** args)
{
    mpz_class a = 23, n;
    if (argc > 1)
    {
        n = args[1];
    }
    mpz_class res;
    {
        Timer timer;
        res = power(a, n);
    }
    std::cout << res << '\n';
}
```

### Time Analysis:

The time complexity is O(*n*), this can be verified with the graph
below.  
![time-analysis](../graph/power.png) \* The jumps in the graph are most
probably caused by how GMP(GNU Multiple Precision Arithmetic Library)
handles a jump in number size. Similar results were achieved in Python
and other Multi Precision Libraries  
\* See Notes at the end of this paper to read about the methodology of
time measurements

## Max

### Aim:

To find out the max element in a list.

### Description:

The algorithm iterates through the list checking each element with the
current max. At the end of the iteration it returns that max.

### Main:

``` py
int max(ls , len):
    int res = ls[1];
    for(i in range(2,len+1)):
        if(res < ls[i]):
            res = ls[i]
    return res
```

### Implementation:

``` cxx
#include "timer.h"
#include <cstdio>
#include <cstdlib>
#include <climits>
#include <functional>
#include <string>

template <typename _T>
_T max(_T* arr, size_t len)
{
    if (len != 0)
    {
        _T res = arr[0];
        for (int i = 1; i < len; ++i)
        {
            if(std::less<_T>{}(res, arr[i]))
                res = arr[i];
        }
        return res;
    }
    return _T();
}

int main(int argc , char **args)
{
    size_t size = 5;
    if(argc > 2)
    {
        size = std::stoul(args[1]);
        srand(std::stoul(args[2]));
    }
    int *arr = new int[size];
    for(int i = 0 ; i < 5; ++i) 
        arr[i] = rand()%INT_MAX;
    int res;
    {
        Timer timer;
        res = max(arr , size);
    }
    printf("%d", res);
    delete [] arr;
}
```

### Time Analysis

The time complexity is O(*n*), this can be verified with the graph
below.  
![time-analysis](../graph/max.png) \* See Notes at the end of this paper
to read about the methodology of time measurements

------------------------------------------------------------------------

## Notes

All times were measured on a Ryzen 3700x processor with jobs running on
all 16 threads. This was done using a few bash scripts and GNUparallel.
The plots were made using gnuplot.  
The process explained step-by-step:

-   The application is written with a timer around the function call and
    nothing else. It is compiled with gcc with -O2 flag. The timer
    reports the time in microseconds to stderr.
    -   g++ linear_search.cc -o out/linear_search.out -O2
-   The application also takes N as a command line argument.
    -   out/sum.out 10000
-   This application is passed as an argument to a bash script along
    with other arguments to format the time and N properly.
    -   time/time.sh out/power-less.out 2000 > /dev/null
-   GNUparallel is used to parallelize this operation of quickness' sake
    and stderr is redirected to a file.
    -   parallel -j16 "time/time.sh out/power-less.out {}" :::
        > {1000..5000000..1000} 2>\> time/power-less
-   gnuplot is used to plot this data along with theoretical big O
    function.
    -   gnuplot> set xlabel "N"; set ylabel "Time(in ms)"; set title
        > "power"; plot "time/powerless" with linespoint, x/4

#### timer.h

``` c
#ifndef TIMER_H
#define TIMER_H

#include <chrono>
#include <cstdio>
#include <ctime>

class Timer
{
    std::chrono::_V2::system_clock::time_point start;
    std::chrono::_V2::system_clock::time_point stop;
    std::chrono::high_resolution_clock         clock;

public:
    Timer()
    {
        start = clock.now();
    }
    ~Timer()
    {
        stop = clock.now();
        auto time = static_cast<double>((stop - start).count());
        fprintf(stderr, "%f" , time/1000);
        fflush(stderr);
    }
};

#endif
```

#### time-search.sh: bash script used for linear-search, binary-search

``` sh
#!/bin/bash

TEMP=/tmp/time-search-$$
trap "rm $TEMP" EXIT ERR SIGINT SIGTERM SIGQUIT

for i in {1..1000}
do 
    $1 $2 $RANDOM ${@:2} 2>> $TEMP > /dev/null
    printf "\n" >> $TEMP
done 
printf "$2    " 1>&2
cat $TEMP | awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' 1>&2
exit 0
```

#### time.sh: bash script used for everything else

``` sh
#!/bin/bash

printf "$2    " 1>&2
$1 ${@:2} > /dev/null
printf "\n" 1>&2
```

#### Every parallel command:

-   Binary Search:

    -   `` sh parallel -j16 "time/time-search.sh out/binary_search.out {}" ::: {1000..5000000..10000} 2>> time/binary_search ``
-   Linear Search:
    -   `` parallel -j16 "time/time-search.sh out/linear_search.out {}" ::: {1000..50000..100} 2>> time/linear_search ``
-   Sum of a list:
    -  `` parallel -j16 "time/time.sh out/sum.out {}" ::: {1000..100000} 2>> time/sum ``
-   Truth Table:
    -  `` parallel -j16 "time/time.sh out/truth_table {}" ::: {2..34} 2>> time/truth_table ``
-   Power:
    -  `` parallel -j16 "time/time.sh out/power-less.out {}" ::: {1000..5000000..1000} 2>> time/power-less ``
-   Max:
    -  `` parallel -j16 "time/time.sh out/max.out {} $RANDOM" ::: {10000..350000000..10000} 2>> time/max ``

#### Every gnuplot command:

-   Binary Search:
    -  `` set xlabel "N"; set ylabel "Time(in ms)"; set title "binary_search"; plot "time/binary_search" with linespoint, log(x)/log(2) - 15 ``
-   Linear Search:
    -  `` set xlabel "N"; set ylabel "Time(in ms)"; set title "linear_search"; plot "time/linear_search" with linespoint, x/5500 ``
-   Sum of a list:
    -  `` set xlabel "N"; set ylabel "Time(in ms)"; set title "sum"; plot "time/sum" with linespoint, x/50 ``
-   Truth Table:
    -  `` set xlabel "N"; set ylabel "Time(in ms)"; set title "truth_table"; plot "time/truth_table" with linespoint, exp(log(2)*(x+2))/600 ``
-   Power:
    -  `` set xlabel "N"; set ylabel "Time(in ms)"; set title "power"; plot "time/powerless" with linespoint, x/5 ``
-   Max:
    -  `` set xlabel "N"; set ylabel "Time(in ms)"; set title "max" ; plot "time/max" with linespoint, x/800 ``

------------------------------------------------------------------------

PDF generated through pandoc.  
Copyright (C) 2021 Himanshu Gupta <guptahman01@gmail.com>  
This work is licensed under a [Creative Commons Attribution-ShareAlike
4.0 International
License](http://creativecommons.org/licenses/by-sa/4.0/).  
All code snippets are licensed under the [GNU
GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html) or later.
