#include <cstddef>
#include <iomanip>
#include <iostream>
#include <stack>
#include <vector>
#include <thread>
#include "include/list.h"

size_t count;

void move(size_t disks, std::vector<int> &source, std::vector<int> &target,
          std::vector<int> &vai)
{
  if (disks == 0)
    return;
  ++count;
  move(disks - 1, source, vai, target);
  target.push_back(source.back());
  source.pop_back();
  move(disks - 1, vai, target, source);
}

int main()
{
  ds::list<int> ls;
  ls.begin();
  std::vector<std::vector<int>> pegs{{}, {}, {}};
  for (int i = 1; i < 65; i++)
    pegs[0].push_back(i);
  move(pegs[0].size(), pegs[0], pegs[2], pegs[1]);
	std::cout << count;
}