// Copyright (C) 2021 Himanshu Gupta
//
// This file is part of Data Structures.
//
// Data Structures is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Data Structures is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SORT_H
#define SORT_H
#include <algorithm>
#include <functional>
#include <utility>

namespace ds
{
template <typename _Iter> void bin_sort(_Iter first, _Iter last);
template <typename _Iter> void count_sort(_Iter first, _Iter last);
template <typename _Iter> void radix_sort(_Iter first, _Iter last);

template <typename _Iterator,
          typename _Compare = std::less<typename _Iterator::value_type>>
void select_sort(_Iterator first, _Iterator last,
                 _Compare compare = std::less<typename _Iterator::value_type>{})
{
  for (auto iter1 = first; iter1 != last; ++iter1)
  {
    auto anchor = iter1;
    for (auto iter2 = iter1; iter2 != last; ++iter2)
    {
      if (compare(*iter2, *iter1))
        anchor = iter2;
    }
    std::iter_swap(anchor, iter1);
  }
}

template <typename _Iterator,
          typename _Compare = std::less<typename _Iterator::value_type>>
void insert_sort(
    _Iterator first, _Iterator last,
    _Compare compare = std::less<typename _Iterator::value_type>{})
{
  if (first == last)
    return;
  for (auto iter1 = first; iter1 != last; ++iter1)
  {
    for (auto iter2 = iter1; iter2 != first; --iter2)
    {
      if (compare(*iter2, *(iter2 - 1)))
        std::iter_swap(iter2, iter2 - 1);
      else
        break;
    }
  }
}

template <typename _Iterator,
          typename _Compare = std::less<typename _Iterator::value_type>>
void bubble_sort(_Iterator first, _Iterator last,
                 _Compare compare = std::less<typename _Iterator::value_type>{})
{
  bool cont = 1;
  while (cont)
  {
    cont = 0;
    for (auto temp = first; temp != last - 1; ++temp)
    {
      if (compare(*(temp + 1), *temp))
      {
        cont = 1;
        std::iter_swap(temp, (temp + 1));
      }
    }
  }
}
} // namespace ds

#endif