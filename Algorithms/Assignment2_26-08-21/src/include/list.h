// Copyright (C) 2021 Himanshu Gupta
//
// This file is part of Data Structures.
//
// Data Structures is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Data Structures is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.

#ifndef LIST_H
#define LIST_H

/**
 * @brief List Implementation
 */
#include <iterator>

namespace ds
{
template <typename _T, typename _Tpointer, typename _Tref> struct ListIter;
template <typename _T> struct ListNode;
template <typename _T> class list;

/**
 * @brief Node of a list
 */
template <typename _T> class ListNode
{
  friend class ListIter<_T, _T *, _T &>;
  friend class ListIter<_T, _T const *, _T const &>;
  friend class list<_T>;

  _T *m_data       = nullptr;
  ListNode *m_next = nullptr;
  ListNode *m_prev = nullptr;
  ListNode *&next() { return m_next; }
  ListNode *&prev() { return m_prev; }
  _T &get() { return *m_data; }

  // Unhooks list element
  ListNode *hook(ListNode *pos)
  {
    this->next()        = pos;
    this->prev()        = pos->prev();
    pos->prev()->next() = this;
    pos->prev()         = this;
    return this;
  }
  ListNode *unhook()
  {
    prev()->next() = next();
    next()->prev() = prev();
    next()         = nullptr;
    prev()         = nullptr;
    return next();
  }
  ListNode(_T const &data, ListNode *next, ListNode *prev)
      : m_data(new _T(data)), m_next(next), m_prev(prev)
  {
  }
  ListNode(ListNode *next, ListNode *prev) : m_next(next), m_prev(prev) {}
  ListNode(_T const &data) : m_data(new _T(data)) {}
  ListNode() = default;
  ~ListNode() noexcept
  {
    delete m_data;
    delete m_next;
  }
};

/**
 * @brief iterator of list
 */
template <typename _T, typename _Tpointer, typename _Tref> struct ListIter
{
  friend class list<_T>;
  friend class ListIter<_T, _T *, _T &>;
  friend class ListIter<_T, _T const *, _T const &>;
  // Iterator Traits
  using iterator          = ListIter<_T, _T *, _T &>;
  using const_iterator    = ListIter<_T, _T const *, _T const &>;
  using node_pointer      = ListNode<_T> *;
  using difference_type   = ptrdiff_t;
  using iterator_category = std::bidirectional_iterator_tag;
  using value_type        = _T;
  using pointer           = _Tpointer;
  using reference         = _Tref;

  // Node Pointer
private:
  node_pointer m_ptr;

public:
  ListIter operator++()
  {
    m_ptr = m_ptr->next();
    return *this;
  }
  ListIter operator++(int)
  {
    auto temp = *this;
    ++(*this);
    return temp;
  }
  ListIter operator--()
  {
    m_ptr = m_ptr->prev();
    return *this;
  }
  ListIter operator--(int)
  {
    auto temp = *this;
    --(*this);
    return temp;
  }
  ListIter operator+(int const &val)
  {
    auto res = *this;
    for (int i = 0; i < val; ++i)
      ++res;
    return res;
  }
  ListIter operator-(int const &val)
  {
    auto res = *this;
    for (int i = 0; i < val; ++i)
      --res;
    return res;
  }
  size_t operator-(ListIter const &iter)
  {

  }
  reference operator*() const { return m_ptr->get(); }
  bool operator==(ListIter const &iter) const
  {
    if (this->m_ptr == iter.m_ptr)
      return true;
    return false;
  }
  bool operator!=(ListIter const &iter) const { return !(iter == *this); }
  explicit ListIter(node_pointer node) : m_ptr(node) {}
  ListIter(iterator const &iter)
      : m_ptr(const_cast<node_pointer>(iter.m_ptr)){};

  // Some getters for convinience
private:
  ListIter next() const { return *this + 1; }
  ListIter prev() const { return *this - 1; }
  node_pointer &get() { return m_ptr; }
};

/**
 * @brief Sequential container with constant time insertion and linear iteration
   @tparam _T Type of Element
 */
template <typename _T> class list
{
public:
  using iterator               = ListIter<_T, _T *, _T &>;
  using reverse_iterator       = std::reverse_iterator<iterator>;
  using const_iterator         = ListIter<_T, _T const *, _T const &>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;
  using Node                   = ListNode<_T>;
  using reference              = typename iterator::reference;
  using const_reference        = typename const_iterator::reference;

private:
  Node *m_head        = nullptr;
  size_t m_node_count = 0;

public:
  // Iterators
  /**
   * @brief Returns an read/write iterator to the first element of the list
   */
  iterator begin() { return iterator(m_head->next()); }

  /**
   * @brief Returns an read/write iterator to one past the last element of
   * the list
   */
  iterator end() { return iterator(m_head); }

  /**
   * @brief Returns a constant iterator to the first element of the list
   */
  const_iterator begin() const { return const_iterator(m_head->next()); }

  /**
   * @brief Returns a constant iterator to one past the last element of
   * the list
   */
  const_iterator end() const { return const_iterator(m_head); }

  /**
   * @brief Returns a reverse read/write iterator to the last element
   * of the list. Iteration is done in reverse order.
   */
  reverse_iterator rbegin() { return reverse_iterator(iterator(end())); }

  /**
   * @brief Returns a reverse read/write iterator to the one before the first
   * element of the list. Iteration is done in reverse order.
   */
  reverse_iterator rend() { return reverse_iterator(iterator(begin())); }

  /**
   * @brief Returns a reverse constant iterator to the last element
   * of the list. Iteration is done in reverse order.
   */
  const_reverse_iterator rbegin() const
  {
    return const_reverse_iterator(const_iterator(end()));
  }

  /**
   * @brief Returns a reverse constant iterator to the one before the first
   * element of the list. Iteration is done in reverse order.
   */
  const_reverse_iterator rend() const
  {
    return const_reverse_iterator(const_iterator(begin()));
  }

  /**
   * @brief
   */
  const_iterator cbegin() const { return begin(); }

  /**
   * @brief Returns a constant iterator to the first element of the list
   */
  const_iterator cend() const { return end(); }

  /**
   * @brief Returns a reverse constant iterator to the last element
   * of the list. Iteration is done in reverse order.
   */
  const_reverse_iterator crbegin() const { return rbegin(); }

  /**
   * @brief Returns a reverse constant iterator to the one before the first
   * element of the list. Iteration is done in reverse order.
   */
  const_reverse_iterator crend() const { return rend(); }

  // Modifiers
  void clear() noexcept
  {
    m_head->prev()->next() = nullptr;
    delete m_head->next();
    m_head->prev() = m_head->next() = m_head;
  }
  iterator insert(const_iterator pos, _T const &value)
  {
    auto ret = iterator((new Node(value))->hook(pos.get()));
    ++m_node_count;
    return ret;
  }
  iterator insert(const_iterator pos, _T &&value)
  {
    auto ret = iterator((new Node(std::move(value)))->hook(pos.get()));
    ++m_node_count;
    return ret;
  }
  iterator erase(iterator pos)
  {
    auto ret = iterator(pos.get()->unhook());
    delete pos.get();
    --m_node_count;
    return ret;
  }
  void push_back(_T const &value) { insert(end(), value); }
  void push_back(_T &&value) { insert(end(), value); }
  void push_front(_T const &value) { insert(begin(), value); }
  void pop_back() { erase(end() - 1); }
  void pop_front() { erase(begin()); }
  reference front() { return *begin(); };
  reference back() { return *rbegin(); };
  const_reference front() const { return *begin(); };
  const_reference back() const { return *rbegin(); };

  // Capacity
  bool empty() const noexcept { return (end() == begin()); }
  bool max_size() const noexcept {}
  bool size() const noexcept { return m_node_count; }
  // Constructors
  list()
  {
    m_head         = new Node();
    m_head->next() = m_head->prev() = m_head;
  }
  list(list const &ls)
  {
    m_head         = new Node();
    m_head->next() = m_head->prev() = m_head;
    for (auto const &iter : ls)
    {
      push_back(iter);
    }
  }
  list(list &&ls) noexcept
  {
    m_head = ls.m_head;
    ls.m_head == nullptr;
  }
  // Dcstr
  ~list() noexcept
  {
    m_head->prev()->next() = nullptr;
    delete m_head;
  }
};
} // namespace ds


#endif