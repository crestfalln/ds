// Copyright (C) 2021 Himanshu Gupta
//
// This file is part of Data Structures.
//
// Data Structures is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Data Structures is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.

#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>

bool paren_match(std::string const &str)
{
  int paren  = 0; // ()
  int brack  = 0; // []
  int brace  = 0; // {}
  bool fflag = 0;
  for (auto const &iter : str)
  {
    switch (iter)
    {
    case '(':
      ++paren;
      break;
    case '[':
      ++brack;
      break;
    case '{':
      ++brace;
      break;
    case ')':
      if (paren > 0)
        --paren;
      else
        fflag = 1;
      break;
    case ']':
      if (brack > 0)
        --brack;
      else
        fflag = 1;
      break;
    case '}':
      if (brace > 0)
        --brace;
      else
        fflag = 1;
      break;
    }
    if (fflag)
      return false;
  }
  if (paren || brace || brack)
    return false;
  return true;
}

int main()
{
  std::string input = "";
	std::ifstream file("list.cc");
  std::cout << paren_match(std::string(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>()));
}