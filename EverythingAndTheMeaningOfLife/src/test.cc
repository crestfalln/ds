// Copyright (C) 2021 Himanshu Gupta
//
// This file is part of Data Structures.
//
// Data Structures is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Data Structures is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.

#include "include/Algorithms/sort.h"
#include "include/DS/avl_tree.h"
#include "include/DS/binary_tree.h"
#include "include/DS/heap.h"
#include "include/DS/list.h"
#include "include/timer_debug.h"
#include <array>
#include <functional>
#include <future>
#include <iostream>
#include <map>
#include <string>
#include <vector>

class something
{
public:
  int         a;
  std::string b;
  bool operator==(something const &other) const { return (a == other.a); }
};
class newInt
{
public:
  int  in;
  bool operator<(newInt const &other) const { return (in < other.in); }
  bool operator==(newInt const &) = delete;
};

struct comp
{
  bool operator()(something const &a, something const &b) { return a.a < b.a; }
};
bool Comp(something const &a, something const &b) { return a.a < b.a; }

int main(int argc, char **args)
{
  // ds::AVLTree<int> avl{61,60,50,16,77,35,14,76,57,84};
  // avl.print(avl.getRoot());
  // std::cout << avl.find(76)->getData();
  // ds::BinTree<newInt>
  // bin_tree({{12,"hello"},{53,"world"},{44,"how"},{55,"are"},{48,"you"}});
  // ds::MaxHeap<int> binTree{61,50,60,16,77,35,14,76,57,84,12};
  // binTree.print(binTree.getRoot());
  // std::cout << binTree.getHeight(binTree.find(77));
  // ds::AVLTree<int>::balance_tree(binTree);
  // binTree.print(std::cout);
  // std::cout << binTree.getHeight(binTree.find(77));
  //  int *arr = new int[10]{61,60,50,16,77,35,14,76,57,84};
  //  ds::heap_sort(arr, 10);
  //  for(int i = 0 ; i < 10 ; ++i)
  //  {
  //  	std::cout << arr[i] << ' ';
  //  	std::cout.flush();
  //  }
  //  delete [] arr;
  //  std::cout << (*mp.find({21})).second;
  //  std::cout << mp.find({21})->getData().in;
  //  ds::list<int> in;
  //  ds::merge_sort(in.begin(), in.end());
  // std::vector<int> vec{12,69,26,47,79,42,32,33,63};
  // std::vector<int> vec{40,41,56,3,22,42,34,57,96,33};
  std::vector<int> vec;
  int              i;
  while (std::cin)
  {
    std::cin >> i;
    vec.push_back(i);
  }
	vec.pop_back();
  auto vec2 = vec;
  // for(auto it : vec)
  // 	std::cout << it << ' ';
	// 	std::cout << std::endl;
  // auto merge = std::async(std::launch::async, [&]()
  //                         { return ds::insert_sort(vec.begin(), vec.end());
  //                         });
  // auto sort  = std::async(std::launch::async,
  //                         [&]() { return std::sort(vec2.begin(), vec2.end()); });
  auto quick = std::async(std::launch::async, [&]()
                          { return ds::quick_sort(vec.begin(), vec.end()); });
  // auto insert = std::async(std::launch::async, [&]()
  //                         { return ds::insert_sort(vec.begin(), vec.end()); });
  // ds::merge_sort(vec.begin(), vec.end());
  // std::sort(vec2.begin(), vec2.end());
  {
    StopWatch watch{};
		// merge.wait();
    quick.wait();
    // sort.wait();
		// insert.wait();

  }
	return 0;
  // ds::quick_sort(vec.begin(), vec.end());
	// return !(vec == vec2);
  // for(auto it : vec)
  // 	std::cout << it << ' ';

  // if (vec != vec2)
  //   return 1;
  // return 0;
}