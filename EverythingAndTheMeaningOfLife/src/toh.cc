// Copyright (C) 2021 Himanshu Gupta
//
// This file is part of Data Structures.
//
// Data Structures is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Data Structures is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <vector>
#include "include/DS/list.h"

size_t count;

void move(size_t disks, std::vector<int> &source, std::vector<int> &target,
          std::vector<int> &vai)
{
  if (disks == 0)
    return;
  ++count;
  move(disks - 1, source, vai, target);
  target.push_back(source.back());
  source.pop_back();
  move(disks - 1, vai, target, source);
}

int main()
{
  ds::list<int> ls;
  ls.begin();
  std::vector<std::vector<int>> pegs{{}, {}, {}};
  for (int i = 1; i < 65; i++)
    pegs[0].push_back(i);
  move(pegs[0].size(), pegs[0], pegs[2], pegs[1]);
	std::cout << count;
}