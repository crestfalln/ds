// Copyright (C) 2021 Himanshu Gupta
//
// This file is part of Data Structures.
//
// Data Structures is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Data Structures is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SORT_H
#define SORT_H
#include "../DS/heap.h"
#include <algorithm>
#include <cstddef>
#include <functional>
#include <iterator>
#include <utility>

namespace ds
{
template <typename _Iter> void bin_sort(_Iter first, _Iter last);
template <typename _Iter> void count_sort(_Iter first, _Iter last);
template <typename _Iter> void radix_sort(_Iter first, _Iter last);

template <typename _Iterator,
          typename _Compare = std::less<typename _Iterator::value_type>>
void select_sort(_Iterator first, _Iterator last,
                 _Compare compare = _Compare{})
{
  for (auto iter1 = first; iter1 != last; ++iter1)
  {
    auto anchor = iter1;
    for (auto iter2 = iter1; iter2 != last; ++iter2)
    {
      if (compare(*iter2, *iter1))
        anchor = iter2;
    }
    std::iter_swap(anchor, iter1);
  }
}

template <typename _Iterator,
          typename _Compare = std::less<typename _Iterator::value_type>>
void insert_sort(_Iterator first, _Iterator last, _Compare compare = _Compare{})
{
  if (first == last)
    return;
  for (auto iter1 = first; iter1 != last; ++iter1)
  {
    for (auto iter2 = iter1; iter2 != first; --iter2)
    {
      if (compare(*iter2, *(iter2 - 1)))
        std::iter_swap(iter2, iter2 - 1);
      else
        break;
    }
  }
}

template <typename _Iterator,
          typename _Compare = std::less<typename _Iterator::value_type>>
void bubble_sort(_Iterator first, _Iterator last,
                 _Compare compare = std::less<typename _Iterator::value_type>{})
{
  bool cont = 1;
  while (cont)
  {
    cont = 0;
    for (auto temp = first; temp != last - 1; ++temp)
    {
      if (compare(*(temp + 1), *temp))
      {
        cont = 1;
        std::iter_swap(temp, (temp + 1));
      }
    }
  }
}

// template <typename _Iterator,
//           typename _Compare = std::less<typename _Iterator::value_type>>
// void heap_sort_pointer(_Iterator first, _Iterator last,
//                        _Compare compare = _Compare{})
// {
//   GenericHeap<typename _Iterator::value_type, _Compare> heap(*first);
//   for (++first, first != last; ++first)
//   {
//     heap.insert(first);
//   }
// }

template <typename _T> void sift_down(_T arr[], int end, int start)
{
  int largest    = start;
  int leftChild  = 2 * start + 1;
  int rightChild = 2 * start + 2;
  if (leftChild < end && arr[leftChild] > arr[largest])
    largest = leftChild;
  if (rightChild < end && arr[rightChild] > arr[largest])
    largest = rightChild;
  if (largest != start)
  {
    std::swap(arr[start], arr[largest]);
    sift_down(arr, end, largest);
  }
}

// main function to do heap sort
template <typename _T> void heap_sort(_T arr[], size_t size)
{
  // make a heap
  for (int i = size / 2 - 1; i >= 0; --i)
    sift_down(arr, size, i);

  for (int i = size - 1; i > 0; --i)
  {
    std::swap(arr[0], arr[i]);
    sift_down(arr, i, 0); // sift down the changed elements
  }
}

template <typename _T, typename _Comp = std::less<typename _T::value_type>>
void quick_sort(_T start, _T end, int pivot = 3, _Comp comp = _Comp{})
{
  if (end - start < 2)
    return;
  auto        pivot_iter = end - 1;
  static auto partition  = [&comp](_T start, _T end, _T pivot_iter)
  {
    auto pivot_val = *pivot_iter;
    auto iter1     = start;
    auto iter2     = end - 1;
    for (; iter1 != end && (iter1 < iter2 ); ++iter1)
    {
      if (!comp(*iter1, *pivot_iter))
      {
        for (; iter2 != start - 1 && (iter1 < iter2); --iter2)
        {
          if (comp(*iter2, *pivot_iter))
          {
            std::iter_swap(iter2, iter1);
            break;
          }
        }
      }
    }
    std::iter_swap(pivot_iter, iter2);
    return iter2;
  };
  pivot_iter = partition(start, end, pivot_iter);
  // for(auto it = start; it != end ; ++it)
  // 	std::cout << *it << ' ';
	// 	std::cout << std::endl;
  quick_sort(start, pivot_iter, pivot, comp);
  quick_sort(pivot_iter + 1, end, pivot, comp);
}

template <typename _T, typename _Comp = std::less<typename _T::value_type>>
void merge_sort(_T RandIt1, _T RandIt2, _Comp comp = _Comp{})
{
  if (RandIt2 - RandIt1 == 1)
    return;
  static_assert(std::is_same<typename _T::iterator_category,
                             std::random_access_iterator_tag>::value,
                "Iterator must be random access");
  static auto merge = [&comp](_T start, _T half, _T end)
  {
    std::vector<typename _T::value_type> vec;
    auto                                 iter1 = start, iter2 = half;
    while (iter1 != half && iter2 != end)
    {
      // if (comp(*iter1, *iter2))
      // {
      //   ++iter1;
      //   continue;
      // }
      // auto temp1 = *iter1;
      // *iter1     = *iter2;
      // for (auto iter3 = iter1 + 1; iter3 != iter2 + 1; ++iter3)
      // {
      //   auto temp2 = *iter3;
      //   *iter3     = temp1;
      //   temp1      = temp2;
      // }
      if (comp(*iter1, *iter2))
      {
        vec.push_back(*iter1);
        ++iter1;
        continue;
      }
      vec.push_back(*iter2);
      ++iter2;
    }
    vec.insert(vec.end(), iter1, half);
    vec.insert(vec.end(), iter2, end);
    int i = 0;
    for (auto iter = start; iter != end; ++iter, ++i)
    {
      *iter = vec[i];
    }
  };
  auto half = RandIt1;
  {
    auto ptrDif = RandIt2 - RandIt1;
    half        = RandIt1 + ptrDif / 2;
  }
  merge_sort(RandIt1, half, comp);
  merge_sort(half, RandIt2, comp);
  //  for (auto print = RandIt1; print != RandIt2; ++print)
  //    std::cout << *print << " ";
  //  std::cout << std::endl;
  merge(RandIt1, half, RandIt2);
}

} // namespace ds

#endif