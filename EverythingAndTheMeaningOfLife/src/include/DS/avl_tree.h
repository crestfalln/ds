// Copyright (C) 2021 Himanshu Gupta
//
// This file is part of Data Structures.
//
// Data Structures is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Data Structures is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AVL_TREE_H
#define AVL_TREE_H
#include "binary_tree.h"

namespace ds
{
template <typename _T, typename _Comp = std::less<_T>,
          typename _Equal = std::equal_to<_T>>
class AVLTree;

template <typename _T, typename _Comp = std::less<_T>,
          typename _Equal = std::equal_to<_T>>
class AVLTreeNode : public BinTreeNode<_T, _Comp, _Equal>
{
protected:
  friend class AVLTree<_T, _Comp, _Equal>;
  using Base = BinTreeNode<_T, _Comp, _Equal>;
  using self = AVLTreeNode<_T, _Comp, _Equal>;
  using Base::BinTreeNode;
  self *parent() { return static_cast<self *>(this->m_parent); }
  self *left() { return static_cast<self *>(Base::left()); }
  self *right() { return static_cast<self *>(Base::right()); }
};

template <typename _T, typename _Comp, typename _Equal>
class AVLTree : public BinTree<_T, _Comp, _Equal>
{
protected:
  using Base = BinTree<_T, _Comp, _Equal>;
  using node = AVLTreeNode<_T, _Comp, _Equal>;
  using self = AVLTree<_T, _Comp, _Equal>;

  static int __ck_balance(node *nd)
  {
    if (nd == nullptr)
      return 0;
    return Base::getHeight(nd->left()) - Base::getHeight(nd->right());
  }
  static node *__balance(node *nd)
  {
    static auto upCast = [](auto x) { return static_cast<node *>(x); };
    if (nd == nullptr)
      return nd;
    auto bf_root = __ck_balance(nd);
    if (abs(bf_root) >= 2)
    {
      __balance(nd->left());
      __balance(nd->right());
      auto  child_right = (bf_root < 0);
      node *child_nd    = upCast(nd->getChildren()[child_right]);
      node *leaf_nd     = upCast(child_nd->getChildren()[child_right]);
      if (leaf_nd == nullptr)
      {
        leaf_nd = upCast(child_nd->getChildren()[!child_right]);
        nd->getChildren()[child_right] = leaf_nd;
        leaf_nd->setParent(nd);
        child_nd->setParent(leaf_nd);
        if(leaf_nd->getChildren()[child_right] != nullptr)
          upCast(leaf_nd->getChildren()[child_right])->setParent(child_nd);
        child_nd->getChildren()[!child_right] = leaf_nd->getChildren()[child_right];
        leaf_nd->getChildren()[child_right] = child_nd;
        std::swap(leaf_nd, child_nd);
      }
      child_nd->setParent(nd->parent());
      nd->setParent(child_nd);
      nd->getChildren()[child_right] = child_nd->getChildren()[!child_right];
      child_nd->getChildren()[!child_right] = nd;
      std::swap(nd, child_nd);
    }
    return nd;
  }

public:
  node *insert(_T const &val)
  {
    auto nd = static_cast<node *>(Base::insert(val));
    if (nd->parent() != nullptr)
    {
      if (std::abs(__ck_balance(nd->parent()->parent())) >= 2)
      {
        auto tmp = __balance(nd->parent()->parent());
        if (tmp->parent() == nullptr)
          this->setRoot(tmp);
      }
    }
    return nd;
  }
  static void balance_tree(BinTree<_T, _Comp, _Equal> &tree)
  {
    tree.setRoot(__balance(static_cast<node *>(tree.getRoot())));
  }

  AVLTree(_T const &val) : Base(val){};
  AVLTree(std::initializer_list<_T> const &vals) : Base(*vals.begin())
  {
    auto tmp = vals.begin();
    for (auto iter = ++tmp; iter != vals.end(); ++iter)
      insert(*iter);
  }
};
} // namespace ds

#endif