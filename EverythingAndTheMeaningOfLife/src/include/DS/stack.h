// Copyright (C) 2021 Himanshu Gupta
//
// This file is part of Data Structures.
//
// Data Structures is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Data Structures is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.

#ifndef STACK_H
#define STACK_H
#include "list.h"

namespace ds
{
template <typename _T, typename _Container = list<_T>> class stack
{
  using Container       = _Container;
  using reference       = _T &;
  using const_reference = _T const &;
  Container m_cont;

public:
  /**
   * @brief Returns true if container is empty and false otherwise
   */
  bool empty() { return m_cont.empty(); }

  /**
   * @brief Returns the current size of the container
   */
  size_t size() { return m_cont.size(); }

  void push(_T const &val) { m_cont.push_back(val); }
  void pop() { m_cont.pop_back(); }
  reference top() { return m_cont.back(); }
  const_reference top() const { return m_cont.back(); }
};
} // namespace ds

#endif