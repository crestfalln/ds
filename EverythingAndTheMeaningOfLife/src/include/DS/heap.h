// Copyright (C) 2021 Himanshu Gupta
//
// This file is part of Data Structures.
//
// Data Structures is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Data Structures is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEAP_H
#define HEAP_H

#include "binary_tree.h"
#include <cstddef>
#include <functional>
#include <initializer_list>
#include <iterator>
#include <utility>

namespace ds
{

template <class _T, typename _Comp = std::greater<_T>, size_t _Max_nodes = 2>
class GenericHeap;

template <class _T, typename _Comp = std::greater<_T>, size_t _Max_nodes = 2>
class GenericHeapNode : GenericTreeNode<_T, _Max_nodes>
{
protected:
  friend class GenericHeap<_T, _Comp, _Max_nodes>;
  using self = GenericHeapNode<_T, _Comp, _Max_nodes>;
  using Base = GenericTreeNode<_T, _Max_nodes>;
  using Base::Base;
  self *shift_up()
  {
    std::swap(this->m_data, static_cast<self *>(this->parent())->m_data);
    return this;
  }
  self *parent() { return static_cast<self *>(Base::parent()); }
};

template <class _T, typename _Comp, size_t _Max_nodes>
class GenericHeap : public GenericTree<_T, 2>
{
protected:
  using Base = GenericTree<_T, 2>;
  using node = GenericHeapNode<_T, _Comp>;
  std::pair<node *, size_t> m_last;
  void                      __next_el()
  {
    auto &[parent, child_index] = m_last;
    if (child_index < parent->getChildren().size() - 1)
    {
      ++child_index;
      return;
    }
    if (parent->parent() == nullptr)
    {
      parent      = static_cast<node *>(parent->getChildren()[0]);
      child_index = 0;
      return;
    }
    if (parent->parent()->getChildren().back() == parent)
    {
      parent = static_cast<node *>(
          static_cast<node *>(parent->parent()->getChildren()[0])
              ->getChildren()[0]);
      child_index = 0;
      return;
    }
    for (int iter = 0; iter < _Max_nodes; ++iter)
    {
      if (parent->parent()->getChildren()[iter] == parent)
      {
        parent = static_cast<node *>(parent->parent()->getChildren()[iter + 1]);
        child_index = 0;
        return;
      }
    }
  }

public:
  node *getRoot() { return static_cast<node *>(this->m_root); }
  GenericHeap(_T const &init_val) : Base(init_val) { m_last = {getRoot(), 0}; }
  GenericHeap(std::initializer_list<_T> const &vals)
      : GenericHeap(*(vals.begin()))
  {
    auto tmp = vals.begin();
    for (++tmp; tmp != vals.end(); ++tmp)
      insert(*tmp);
  }
  node *insert(_T const &val)
  {
    auto [parent, child_index]         = m_last;
    parent->getChildren()[child_index] = new node(val, parent);
    sift_down(parent);
    __next_el();
    return parent;
  }
  static bool sift_down(node *root)
  {
    if(root == nullptr)
      return 1;
    node *swap = root;
    for (auto iter : root->getChildren())
    {
      if (iter == nullptr)
        continue;
      if (!_Comp{}(root->getData(), iter->getData()))
        swap = static_cast<node *>(iter)->shift_up();
    }
    if (swap != root)
      return sift_down(root->parent());
    return 1;
  }
};

template<typename _T>
using MaxHeap = GenericHeap<_T>;

} // namespace ds
#endif