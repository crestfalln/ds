// Copyright (C) 2021 Himanshu Gupta
//
// This file is part of Data Structures.
//
// Data Structures is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Data Structures is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.

#ifndef UBT_H
#define UBT_H

#include "iostream"
#include <initializer_list>
#include <sstream>
#include <vector>

template <typename _T> struct BinarySearchTreeNode
{
  _T m_data;
  BinarySearchTreeNode *right = nullptr;
  BinarySearchTreeNode *left  = nullptr;
  BinarySearchTreeNode(_T const &data)
  {
    m_data = data;
    right  = nullptr;
    left   = nullptr;
  }
  ~BinarySearchTreeNode()
  {
    if (right != nullptr)
      delete right;
    if (left != nullptr)
      delete left;
  }
  _T operator*() { return m_data; }
};

template <typename _T>
void create(BinarySearchTreeNode<_T> &node, int indent = 0)
{
  bool truth = 0;
  std::cout << "Left? (" << indent << ")\n";
  std::cin >> truth;
  _T val;
  if (truth)
  {
    std::cin >> val;
    node.left = new BinarySearchTreeNode<_T>(val);
    create(*node.left, indent + 1);
  }
  truth = 0;
  std::cout << "Right? (" << indent << ")\n";
  std::cin >> truth;
  if (truth)
  {
    std::cin >> val;
    node.left = new BinarySearchTreeNode<_T>(val);
    create(*node.left, indent + 1);
  }
}

template <typename _T> struct BinarySearchTree
{
  BinarySearchTreeNode<_T> m_root;
  BinarySearchTree(_T const &root) : m_root(root) {}
  BinarySearchTreeNode<_T> *find(_T const &val)
  {
    auto ans = find_impl(val, &m_root);
    if (ans->m_data != val)
      return nullptr;
    return ans;
  }
  bool insert(_T const &val)
  {
    auto pos = find_impl(val, &m_root);
    if (pos->m_data == val)
      return false;
    if (pos->m_data > val)
      pos->left = new BinarySearchTreeNode<_T>(val);
    else
      pos->right = new BinarySearchTreeNode<_T>(val);
    return true;
  }
  bool insert(std::initializer_list<_T> list)
  {
    bool result = 1;
    for (auto it : list)
    {
      if (!insert(it))
        result = 0;
    }
    return result;
  }
  void print() { print_impl(&m_root); }
  int depth(BinarySearchTreeNode<_T> *node)
  {
    if (node == nullptr)
      return -1;
    auto rightH = depth(node->right) + 1;
    auto leftH  = depth(node->left) + 1;
    return (rightH > leftH) ? rightH : leftH;
  }

private:
  BinarySearchTreeNode<_T> *find_impl(_T const &val,
                                      BinarySearchTreeNode<_T> *node)
  {
    if (node->m_data == val)
      return node;
    if (node->m_data > val)
    {
      if (node->left == nullptr)
        return node;
      return find_impl(val, node->left);
    }
    if (node->right == nullptr)
      return node;
    return find_impl(val, node->right);
  }

public:
  void print_impl(BinarySearchTreeNode<_T> *node, int indent = 0)
  {
    if (node == nullptr)
      return;
    print_impl(node->right, indent + 1);
    indent += 3;
    for (int i = 0; i < indent; ++i)
      std::cout << ' ';
    std::cout << node->m_data << "\n";
    print_impl(node->left, indent + 1);
  }
};

#endif