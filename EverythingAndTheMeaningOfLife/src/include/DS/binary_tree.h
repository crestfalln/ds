// Copyright (C) 2021 Himanshu Gupta
//
// This file is part of Data Structures.
//
// Data Structures is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Data Structures is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BINARY_TREE_H
#define BINARY_TREE_H

#include "tree.h"

namespace ds
{
template <typename _T, typename _Comp = std::less<_T>,
          typename _Equal = std::equal_to<_T>>
class BinTree;
template <typename _T, typename _Comp = std::less<_T>,
          typename _Equal = std::equal_to<_T>>
class BinTreeNode : public GenericTreeNode<_T, 2>
{
protected:
  using self     = BinTreeNode<_T, _Comp, _Equal>;
  using Base     = GenericTreeNode<_T, 2>;
  using iterator = typename Base::iterator;
  BinTreeNode(_T const &val, self *parent) : Base(val, parent) {}
  friend class BinTree<_T, _Comp, _Equal>;
  self *left() { return static_cast<self *>(this->m_children[0]); }
  self *right() { return static_cast<self *>(this->m_children[1]); }
  self *parent() { return static_cast<self *>(this->m_parent); }
  void  setParent(self *par) { this->m_parent = par; }
  void  setLeft(self *lt) { this->m_children[0] = lt; }
  void  setRight(self *rt) { this->m_children[1] = rt; }

public:
};

template <typename _T, typename _Comp, typename _Equal>
class BinTree : public GenericTree<_T, 2>
{
protected:
  using node     = BinTreeNode<_T, _Comp, _Equal>;
  using Base     = GenericTree<_T, 2>;
  using iterator = typename Base::iterator;

public:
  node                *getRoot() { return static_cast<node *>(Base::getRoot()); }
  typename Base::node *insert(std::vector<_T> const &,
                              typename Base::node *)           = delete;
  typename Base::node *traverse(typename Base::node *, size_t) = delete;
  node                *find(_T const &val) { return find_impl(val, getRoot()); }
  std::pair<iterator, iterator> eqaul_range(_T const &val)
  {
    return {find(val)->m_data.begin(), find(val)->m_data.end()};
  }
  node *insert(_T const &val)
  {
    auto nd = find(val);
    if (_Equal{}(nd->getData(), val))
    {
      nd->pushList(val);
      return nd;
    }
    if (_Comp{}(nd->getData(), val))
    {
      nd->setLeft(new node(val, nd));
      return nd->left();
    }
    nd->setRight(new node(val, nd));
    return nd->right();
  }
  BinTree(_T const &root_val) : Base(root_val){};
  BinTree(std::initializer_list<_T> const &vals) : BinTree(*(vals.begin()))
  {
    auto tmp = vals.begin();
    for (auto iter = ++tmp; iter != vals.end(); ++iter)
      insert(*iter);
  }

protected:
  node *find_impl(_T const &val, node *nd)
  {
    if (_Equal{}(nd->getData(), val))
      return nd;
    if (_Comp{}(nd->getData(), val))
    {
      if (nd->left() == nullptr)
        return nd;
      return find_impl(val, nd->left());
    }
    if (nd->right() == nullptr)
      return nd;
    return find_impl(val, nd->right());
  }
};

} // namespace ds

#endif