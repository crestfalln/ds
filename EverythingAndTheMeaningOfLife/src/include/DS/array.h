// Copyright (C) 2021 Himanshu Gupta
//
// This file is part of Data Structures.
//
// Data Structures is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Data Structures is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.

#ifndef ARRAY_H
#define ARRAY_H

#include <array>
#include <cstddef>
#include <stdexcept>
namespace ds
{
template <typename _T, size_t _Size> class Array
{
protected:
  using value_type      = _T;
  using pointer         = _T *;
  using reference       = _T &;
  using const_reference = _T const &;
  pointer data;
  long    cur;

public:
  Array() : data(new value_type[_Size]), cur(-1) {}
  ~Array() noexcept { delete[] data; }
  size_t          size() const { return _Size; }
  const_reference back() const { return back(); }
  reference       back() { return data[cur]; }
  const_reference front() const { return front(); }
  reference       front() { return *data; }
  bool            empty() const { return (cur == -1); }
  void            push_back(const_reference val)
  {
    if (cur >= size() - 1)
      throw std::out_of_range("Cannot push into array, out of memory");
    data[++cur] = val;
  }
  void pop_back()
  {
    if (cur == 0)
      return;
    --cur;
  }
  reference operator[](size_t const &index)
  {
    if (index > cur)
      throw std::out_of_range("Index is out of range");
    return data[index];
  }
  const_reference operator[](size_t const &index) const
  {
    return this->operator[](index);
  }
};
} // namespace ds

#endif