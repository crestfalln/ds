// Copyright (C) 2021 Himanshu Gupta
//
// This file is part of Data Structures.
//
// Data Structures is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Data Structures is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.

#ifndef TREE_H
#define TREE_H

//#include "array.h"
#include "list.h"
#include <algorithm>
#include <array>
#include <iostream>
#include <vector>

namespace ds
{
template <typename _T, size_t _Max_Nodes> class GenericTree;
template <typename _T, size_t _Max_Nodes> class GenericTreeNode
{
protected:
  friend class GenericTree<_T, _Max_Nodes>;
  using self     = GenericTreeNode<_T, _Max_Nodes>;
  using list     = ds::list<_T *>;
  using iterator = typename list::iterator;

  list                           m_data;
  self                          *m_parent;
  std::array<self *, _Max_Nodes> m_children;
  GenericTreeNode(_T const &data, self *parent)
      : m_data({new _T(data)}), m_parent(parent)
  {
    m_children.fill(nullptr);
  }
  ~GenericTreeNode() noexcept
  {
    for (auto iter : m_data)
      delete iter;
    for (auto iter : m_children)
      delete iter;
  }
  constexpr size_t max_childs() const { return _Max_Nodes; }
  size_t           child_count() const
  {
    int count = 0;
    for (auto iter : m_children)
    {
      if (iter != nullptr)
        ++count;
    }
    return count;
  }
  auto &getChildren() { return m_children; }
  auto &getParent() { return m_parent; }
  auto  parent() { return m_parent; }
  void  setParent(self *par) { m_parent = par; }
  auto  pushList(_T const &val) { return m_data.push_back(new _T(val)); }

public:
  virtual self *traverse(size_t child) { return m_children[child]; }
  _T            operator*() { return **(m_data.begin()); }
  _T            getData() { return **(m_data.begin()); }
};

template <typename _T, size_t _Max_Nodes> class GenericTree
{
protected:
  using node     = GenericTreeNode<_T, _Max_Nodes>;
  using self     = GenericTree<_T, _Max_Nodes>;
  using iterator = typename node::iterator;
  node *m_root;

public:
  template <typename _Out> void t_print(node *root, _Out &os, int space = 0)
  {
    if (root == nullptr)
      return;
    space += 3;
    auto printOut = [&](node *nd)
    {
      for (int i = 0; i < space; ++i)
        os << " ";
      os << nd->getData() << "\n";
      os.flush();
    };
    bool done = 0;
    for (auto iter : root->getChildren())
    {
      t_print(iter, os, space);
      if (!done)
      {
        printOut(root);
        done = true;
      }
    }
    return;
  }
  void print(std::ostream &os)
  {
    t_print(m_root, os);
  }
  node *getRoot() const { return m_root; }
  void  setRoot(node *new_root) { m_root = new_root; }
  node *insert(std::vector<_T> const &vals, node *pos)
  {
    if (pos == nullptr)
      return nullptr;
    node *ret = nullptr;
    for (int i = 0; i < pos->m_children.size(); ++i)
    {
      node *to_put = nullptr;
      if (i < vals.size())
      {
        to_put = new node(vals[i], pos);
        ret    = to_put;
      }
      pos->m_children[i] = to_put;
    }
    return ret;
  }
  node *del(node *pos) noexcept
  {
    if (pos == nullptr)
      return nullptr;
    auto parent = pos->m_parent;
    delete pos;
    return parent;
  }
  node *traverse(node *pos, size_t child) const
  {
    if (child >= pos->child_count())
      return nullptr;
    return pos->traverse(child);
  }
  static int getDepth(node *pos)
  {
    if (pos == nullptr)
      return -1;
    size_t counter = 0;
    while (pos->m_parent != nullptr)
    {
      pos = pos->m_parent;
      ++counter;
    }
    return counter;
  }
  static int getHeight(node *pos)
  {
    if (pos == nullptr)
      return -1;
    std::array<int, _Max_Nodes> heights;
    heights.fill(0);
    for (int i = 0; i < pos->m_children.size(); ++i)
      heights[i] = getHeight(pos->m_children[i]);
    return *(std::max_element(heights.begin(), heights.end())) + 1;
  }
  GenericTree(_T const &root_val) : m_root(new node(root_val, nullptr)) {}
  ~GenericTree() noexcept { delete m_root; }
};
} // namespace ds

#endif